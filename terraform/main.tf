# Configure the OpenStack Provider
provider "openstack" {
  user_domain_name = "jalasoft"
  user_name = "carmen.villcazani"
  password = var.user_password
  auth_url = "https://10.28.248.101:5000/v3"
  tenant_name = "jalasoft_devops_101"
  insecure = true
  region = "jala-dc01"
}

# Create keypair
resource "openstack_compute_keypair_v2" "key_pair" {
  name = "kecavi01"
  public_key = file("${var.ssh_key_file}.pub")
}

# Create security group
resource "openstack_compute_secgroup_v2" "security_group" {
  name = "sgcavi01"
  description = "cavi security group"

  rule {
    from_port = 22
    to_port = 22
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }

  rule {
    from_port = 8153
    to_port = 8153
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }

}

# Create a gocd server
resource "openstack_compute_instance_v2" "cd_server" {
  name = "${var.instance_prefix}-01"
  image_name = var.image
  flavor_name = var.flavor
  key_pair = openstack_compute_keypair_v2.key_pair.name
  security_groups = ["${openstack_compute_secgroup_v2.security_group.name}","default"]

  provisioner "local-exec" {
    command = "sed -e 's,GOCD_SERVER_IP,${openstack_compute_instance_v2.cd_server.network.0.fixed_ip_v4},g' ../ansible/inventory.yml > inventory.gocd.yml"
  }

  # This is to ensure SSH comes up before we run the local exec.
  provisioner "remote-exec" { 
    inline = ["ls -la"]

    connection {
      type = "ssh"
      host = openstack_compute_instance_v2.cd_server.network.0.fixed_ip_v4
      user = var.ssh_user_name
      private_key = file("${var.ssh_key_file}")
    }
  }

  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventory.gocd.yml --extra-vars \"docker_servers=cicdserver\" --private-key ${var.ssh_key_file} ../ansible/docker.yml"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.gocd.yml --private-key ${var.ssh_key_file} ../ansible/gocd-docker.yml"
  }

}

# Create a sonar server
resource "openstack_compute_instance_v2" "sonar_server" {
  name = "${var.instance_prefix}-02"
  image_name = var.image
  flavor_name = var.flavor
  key_pair = openstack_compute_keypair_v2.key_pair.name
  security_groups = ["${openstack_compute_secgroup_v2.security_group.name}","default"]

  provisioner "local-exec" {
    command = "sed -e 's,SONAR_SERVER_IP,${openstack_compute_instance_v2.sonar_server.network.0.fixed_ip_v4},g' ../ansible/inventory.yml > inventory.sonar.yml"
  }

  # This is to ensure SSH comes up before we run the local exec.
  provisioner "remote-exec" { 
    inline = ["ls -la"]

    connection {
      type = "ssh"
      host = openstack_compute_instance_v2.sonar_server.network.0.fixed_ip_v4
      user = var.ssh_user_name
      private_key = file("${var.ssh_key_file}")
    }
  }
  
  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventory.sonar.yml --extra-vars \"docker_servers=sonarserver\" --private-key ${var.ssh_key_file} ../ansible/docker.yml"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.sonar.yml --private-key ${var.ssh_key_file} ../ansible/sonarqube.yml"
  }
}

# Create a testing server
resource "openstack_compute_instance_v2" "testing_server" {
  name = "${var.instance_prefix}-03"
  image_name = var.image
  flavor_name = var.flavor
  key_pair = openstack_compute_keypair_v2.key_pair.name
  security_groups = ["${openstack_compute_secgroup_v2.security_group.name}","default"]

  provisioner "local-exec" {
    command = "sed -e 's,TESTING_SERVER_IP,${openstack_compute_instance_v2.testing_server.network.0.fixed_ip_v4},g' ../ansible/inventory.yml > inventory.testing.yml"
  }

  # This is to ensure SSH comes up before we run the local exec.
  provisioner "remote-exec" { 
    inline = ["ls -la"]
    connection {
      type = "ssh"
      host = openstack_compute_instance_v2.testing_server.network.0.fixed_ip_v4
      user = var.ssh_user_name
      private_key = file("${var.ssh_key_file}")
    }
  }
  
  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventory.testing.yml --extra-vars \"docker_servers=testingserver\" --private-key ${var.ssh_key_file} ../ansible/docker.yml"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.testing.yml --private-key ${var.ssh_key_file} ../ansible/testing.yml"
  }
}

# Create a docker swarm server
resource "openstack_compute_instance_v2" "swarm_server" {
  name = "${var.instance_prefix}-04"
  image_name = var.image
  flavor_name = var.flavor
  key_pair = openstack_compute_keypair_v2.key_pair.name
  security_groups = ["${openstack_compute_secgroup_v2.security_group.name}","default"]

  provisioner "local-exec" {
    command = "sed -e 's,DOCKER_SWARM_SERVER_IP,${openstack_compute_instance_v2.swarm_server.network.0.fixed_ip_v4},g' ../ansible/inventory.yml > inventory.swarm.yml"
  }
  # This is to ensure SSH comes up before we run the local exec.
  provisioner "remote-exec" { 
    inline = ["ls -la"]
    connection {
      type = "ssh"
      host = openstack_compute_instance_v2.swarm_server.network.0.fixed_ip_v4
      user = var.ssh_user_name
      private_key = file("${var.ssh_key_file}")
    }
  }
    
  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventory.swarm.yml --extra-vars \"docker_servers=dockerswarmserver\" --private-key ${var.ssh_key_file} ../ansible/docker.yml"
  }

  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventory.swarm.yml --extra-vars \"docker_servers=dockerswarmserver\" --private-key ${var.ssh_key_file} ../ansible/enable-tcp.yml"
  }

}
