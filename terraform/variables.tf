variable "user_password" {
  type = string
}

variable "image" {
  default = "ubuntu-18.04-amd64-server_20191107"
}

variable "flavor" {
  default = "student.2.4R"
}

variable "ssh_key_file" {
  default = "ssh_path_here"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "instance_count" {
  default = 1
}

variable "instance_prefix" {
  default = "VMCAVI-CICD"
}