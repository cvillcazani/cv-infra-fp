#!/bin/bash
# Get Agent Auto Register Key, this value will be provided to gocd agents

config_content=$(curl -k --silent 'http://localhost:8153/go/api/admin/config.xml')
key=$(grep -oP '(?<=agentAutoRegisterKey=").*?(?=")' <<< $config_content)
echo $key